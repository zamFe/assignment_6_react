import React from 'react'
import styles from "./style/header.module.css"
import {useHistory} from "react-router-dom";
import {useUserContext} from "../contexts/UserContext";

export const Header = () => {

    console.log('Header.render')

    const [ user ] = useUserContext()
    const history = useHistory()

    const HandleProfileClicked = () => {
        history.push("/profile")
    }

    const HandleHeaderImgClicked = () => {
        if(user !== null) {
            history.push("/translate")
        } else {
            history.push("/")
        }

        //history.push("/") // This will redirect user to translate if logged in.
    }

    return (
        <>
            <header>
                <a className={styles.logoContainer} onClick={ HandleHeaderImgClicked }>
                    <img className={styles.logoImg} src="/lostInTranslation/Logo.png" width="50px" height="50px"  alt="Logo Image"/>
                    <h2 className={styles.logoTitle}>Lost in Translation</h2>
                </a>
                { user !== null &&
                    <div className={styles.headerProfileDisplay} onClick={ HandleProfileClicked }>
                        <h3 className={styles.profileName}>{ user }</h3>
                        <i className="fas fa-user-circle fa-2x"/>
                    </div>
                }
            </header>
        </>
    );
}
