import React from 'react'
import './App.css'
import { Startup } from './components/Startup'
import { Translator } from './components/Translator'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Profile from "./components/Profile";
import ProtectedRoute from "./hoc/ProtectedRoute"
import { Header } from './components/Header'

function App() {
    console.log("Switch")
  return (
    <BrowserRouter>
        {/*
            <nav>
                <NavLink to="/">Home</NavLink>
                <NavLink to="/translate">Translate</NavLink>
                <NavLink to="/profile">Profile</NavLink>
              </nav>
        */}
      <Header/>
      <main>
        <Switch>
          <Route exact path="/"  component={Startup}/>
          <ProtectedRoute path={"/translate"} component={Translator}/>
          <ProtectedRoute path={"/profile"} component={Profile}/>
        </Switch>
      </main>
    </BrowserRouter>
  );
}

export default App
