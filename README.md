# Assignment 3 - ASL Translator using React

We have build an online American Sign Language Translator as a Single Page Application using React Framework.

![start page](https://gitlab.com/zamFe/assignment_6_react/-/raw/main/translation_image.png)

### How to run:
- npm
- Node.js
- Heroku CLI (For Pushing Project To Heroku For Hosting)
- React (w/ react-router-dom)
- Vite

## Project setup
```
npm install

Dev:
npm run dev

Build:
npm build
```

### Hosted Solutions
https://arcane-retreat-14990.herokuapp.com/
https://lost-in-translation-zamfe.herokuapp.com/

### Component Tree
https://www.figma.com/file/rERBB5D8vJdvzWivpviPas/Assignment-5-Noroff-React?node-id=0%3A1

### Translation Log API Server
https://noroff-assignment-5-api.herokuapp.com/translations

